package pl.pmajewski.dropgame.service;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

import java.nio.channels.Pipe;

import pl.pmajewski.dropgame.model.GameActor;
import pl.pmajewski.dropgame.model.Obstacle;
import pl.pmajewski.dropgame.stage.mediator.LocalMediator;
import pl.pmajewski.dropgame.stage.mediator.dto.StopWorldMessage;
import pl.pmajewski.dropgame.utils.DebugLogger;

public class WorldContactListener implements ContactListener {

    @Override
    public void beginContact(Contact contact) {
        DebugLogger.debug("contact -> A="+contact.getFixtureA().getUserData().getClass()+" B="+contact.getFixtureB().getUserData().getClass(), "CONTACT_LISTENER", "BEGIN");

        if(Obstacle.class.isInstance(contact.getFixtureA().getUserData()) || Obstacle.class.isInstance(contact.getFixtureB().getUserData())) {
            DebugLogger.debug("", "CONTACT_LISTENER", "BEGIN", "COLLISION", "PLAYER-OBSTACLE");
            LocalMediator.getInstance().emit(new StopWorldMessage());
            contact.getFixtureA().getBody().getWorld().clearForces();
        }
    }

    @Override
    public void endContact(Contact contact) {
        DebugLogger.debug("contact -> "+contact, "CONTACT_LISTENER", "END");
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
//        DebugLogger.debug("contact -> "+contact, "CONTACT_LISTENER", "PRE_SOLVE");
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
//        DebugLogger.debug("contact -> "+contact, "CONTACT_LISTENER", "POST_SOLVE");
    }
}
