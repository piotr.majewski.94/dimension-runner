package pl.pmajewski.dropgame.stage.game.mediator;

import pl.pmajewski.dropgame.stage.game.mediator.dto.Message;

public interface Colleague {

    <T extends Message> void notify(T message);
}
