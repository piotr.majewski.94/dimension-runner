package pl.pmajewski.dropgame.stage.game.mediator;


import pl.pmajewski.dropgame.stage.game.mediator.dto.Message;

/**
 * Multicomponent communicator
 */
public interface Mediator {

    <T extends Message> void register(Colleague colleague, Class<T> clazz);

    <T extends Message> void register(Colleague colleague, Class<T>... clazz);

    <T extends Message> void emit(T message);
}
