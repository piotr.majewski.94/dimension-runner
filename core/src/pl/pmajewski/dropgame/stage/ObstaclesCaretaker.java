package pl.pmajewski.dropgame.stage;

import java.util.LinkedList;
import java.util.Optional;

import pl.pmajewski.dropgame.model.Obstacle;

public class ObstaclesCaretaker {

    private LinkedList<Obstacle> obstacles = new LinkedList<>();

    public void add(Obstacle obstacle) {
        obstacles.add(obstacle);
    }

    public Obstacle getLast() {
        return obstacles.peekLast();
    }
}
