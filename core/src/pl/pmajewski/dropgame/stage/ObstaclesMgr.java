package pl.pmajewski.dropgame.stage;

import com.badlogic.gdx.math.Vector2;

import java.util.Random;

import pl.pmajewski.dropgame.model.GameActor;
import pl.pmajewski.dropgame.model.Obstacle;
import pl.pmajewski.dropgame.model.factory.ObstacleFactory;
import pl.pmajewski.dropgame.utils.DebugLogger;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public class ObstaclesMgr {

    private static final float OBSTACLE_DETECTOR_RAY = GlobalProperties.APP_WIDTH;
    private Random random = new Random();

    private GameStage gameStage;

    private ObstacleFactory factory;
    private ObstaclesCaretaker caretaker;

    public ObstaclesMgr(GameStage gameStage) {
        this.factory = new ObstacleFactory(gameStage.getWorld());
        this.caretaker = new ObstaclesCaretaker();
        this.gameStage = gameStage;
    }

    public void notifyPlayerPosition(Vector2 position) {
        Obstacle last = caretaker.getLast();
        while(last == null || position.x + OBSTACLE_DETECTOR_RAY > last.getBodyPosition().x ) {
            if(last == null) {
                initObstacle(gameStage.getPlayer(), gameStage.getPlayer().getBodyWidth());
            } else {
                initObstacle(last);
            }

            last = caretaker.getLast();
        }
    }

    private void initObstacle(GameActor lastObstacle) {
        initObstacle(lastObstacle, 0f);
    }

    private void initObstacle(GameActor lastObstacle, float additionalShift) {
        Obstacle obstacle = createObstacle(lastObstacle);
        shiftObstacle(obstacle, lastObstacle, additionalShift);
    }

    private void shiftObstacle(GameActor obstacle, GameActor lastObstacle, float overShift) {
        float shift = computeSpanBegining(obstacle, lastObstacle) + randShift() + overShift;
        DebugLogger.debug("newObstaclePosition="+obstacle.getBodyPosition()+" newObstacleShift="+shift, "OBSTACLE_MGR", "INIT", "PRE_SHIFT");
        obstacle.shiftHorizontaly(shift);
        DebugLogger.debug( "newObstaclePosition="+obstacle.getBodyPosition(), "OBSTACLE_MGR", "INIT", "POST_SHIFT");
    }

    private float computeSpanBegining(GameActor obstacle, GameActor lastObstacle) {
        return lastObstacle.getBodyWidth() / 2f + obstacle.getBodyWidth() / 2f;
    }

    private float randShift() {
        return GlobalProperties.OBSTACLE_SHIFT_MIN + random.nextFloat() * (GlobalProperties.OBSTACLE_SHIFT_MAX - GlobalProperties.OBSTACLE_SHIFT_MIN);
    }

    private Obstacle createObstacle(GameActor lastObstacle) {
        Obstacle obstacle = factory.create(lastObstacle.getBodyPosition().x, randOverPipe());
        caretaker.add(obstacle);
        gameStage.addActor(obstacle);

        return  obstacle;
    }

    /**
     * Randomize top down obstacle position
     * Over/Down pipe
     */
    private boolean randOverPipe() {
        return Math.random() > 0.5d;
    }
}
