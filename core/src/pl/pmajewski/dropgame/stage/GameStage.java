package pl.pmajewski.dropgame.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;

import lombok.Getter;
import pl.pmajewski.dropgame.model.PipeModel;
import pl.pmajewski.dropgame.model.PlayerModel;
import pl.pmajewski.dropgame.model.PositionListener;
import pl.pmajewski.dropgame.service.WorldContactListener;
import pl.pmajewski.dropgame.stage.game.mediator.Colleague;
import pl.pmajewski.dropgame.stage.game.mediator.LocalMediator;
import pl.pmajewski.dropgame.stage.game.mediator.dto.Message;
import pl.pmajewski.dropgame.stage.game.mediator.dto.StopWorldMessage;
import pl.pmajewski.dropgame.utils.DebugLogger;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public class GameStage extends Stage implements PositionListener, Colleague {

    @Getter
    private World world;
    private float accumulator = 0f;
    @Getter
    private Box2DDebugRenderer debugRender;

    @Getter
    private PlayerModel player;
    private ObstaclesMgr obstaclesMgr;
    @Getter
    private PipeModel pipe;

    private boolean stopWorld = false;

    public GameStage() {
        super(new FillViewport(GlobalProperties.APP_WIDTH, GlobalProperties.APP_HEIGHT,
                new OrthographicCamera(GlobalProperties.APP_WIDTH, GlobalProperties.APP_HEIGHT)));

        Gdx.input.setInputProcessor(this);
        debugRender = new Box2DDebugRenderer(true, true, true, true, true, true);

        initWorld();
        initPlayer();
        initPipe();
        initObstacles();
        LocalMediator.getInstance().register(this, StopWorldMessage.class);
    }

    private void initObstacles() {
        this.obstaclesMgr = new ObstaclesMgr(this);
    }

    private void initPipe() {
        this.pipe = new PipeModel(world, new Vector2(GlobalProperties.PIPE_X, GlobalProperties.PIPE_Y), new Vector2(GlobalProperties.PIPE_WIDTH, GlobalProperties.PIPE_HEIGHT));
        addActor(pipe);
    }

    private void initPlayer() {
        player = new PlayerModel(world, this);
        addActor(player);
    }

    private void initWorld() {
        this.world = new World(GlobalProperties.WORLD_GRAVITY, true);
        world.setContactListener(new WorldContactListener());
        World.setVelocityThreshold(0f);
    }

    private void setCameraPosition() {
        getCamera().position.x = player.getX() + GlobalProperties.CAM_SHIFT_HORIZONTAL;
        getCamera().position.y = GlobalProperties.APP_MIDDLE_Y - 2;
    }

    @Override
    public void act(float delta) {
        setCameraPosition();
        super.act(delta);
        if(!stopWorld) {
            actWorld(delta);
        }
    }

    private void actWorld(float delta) {
        this.accumulator += delta;
        if(accumulator >= delta) {
            world.step(GlobalProperties.TIME_STEP, 6, 2);
            this.accumulator -= GlobalProperties.TIME_STEP;
        }
    }

    @Override
    public boolean keyDown(int keyCode) {
        if(Input.Keys.SPACE == keyCode) {
            player.reflect();
        }

        if(Input.Keys.S == keyCode) {
            stopWorld = true;
        }
        return super.keyDown(keyCode);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        DebugLogger.debug("x="+screenX+" y="+screenY+" pointer="+pointer+" button="+button, "STAGE", "TOUCH");

        player.reflect();
        return super.touchDown(screenX, screenY, pointer, button);
    }

    public Box2DDebugRenderer getBox2DDebug() {
        return this.debugRender;
    }

    @Override
    public void notify(Vector2 position) {
        obstaclesMgr.notifyPlayerPosition(position);
        pipe.transformPositionY(position.x);
    }

    @Override
    public <T extends Message> void notify(T message) {
        DebugLogger.debug("GET MESSAGE "+message, "GAME_STAGE");

        if(message instanceof StopWorldMessage) {
            DebugLogger.debug("GET STOP WORLD MESSAGE", "GAME_STAGE");
            stopWorld = true;
        }
    }
}