package pl.pmajewski.dropgame.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import pl.pmajewski.dropgame.utils.GlobalProperties;

public class PipeModel extends GameActor {

    private Vector2 size;
    private ShapeRenderer sr;

    public PipeModel(World world, Vector2 position, Vector2 size) {
        super(world);
        this.size = size;
        createBody(world, position);

        sr = new ShapeRenderer();
        sr.setAutoShapeType(true);
    }

    private void createBody(World world, Vector2 position) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(position);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(size.x / 2, size.y / 2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = GlobalProperties.OBSTACLE_DENSITY;
        fixtureDef.restitution = 0f;
        fixtureDef.friction = 0f;
        fixtureDef.isSensor = true;

        Body body = world.createBody(bodyDef);
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(this);
        body.setUserData(this);

        shape.dispose();
        this.body = body;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();

        Color color = new Color(Color.CYAN);
        sr.setProjectionMatrix(batch.getProjectionMatrix());
        sr.setColor(color);

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        sr.begin(ShapeRenderer.ShapeType.Filled);

        sr.rect(body.getPosition().x - getBodyWidth() / 2, body.getPosition().y - getBodyHeight() / 2, getBodyWidth(), getBodyHeight());
        sr.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
        Gdx.gl.glLineWidth(1f);

        batch.begin();
    }

    public void transformPositionY(float x) {
        body.setTransform(x, body.getPosition().y, body.getAngle());
    }

    @Override
    public Float getBodyWidth() {
        return size.x;
    }

    @Override
    public Float getBodyHeight() {
        return size.y;
    }
}
