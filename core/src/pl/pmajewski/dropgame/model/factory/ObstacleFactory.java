package pl.pmajewski.dropgame.model.factory;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import pl.pmajewski.dropgame.model.Obstacle;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public class ObstacleFactory {

    private World world;
    private int obstaclesCounter;

    public ObstacleFactory(World world) {
        this.world = world;
    }

    public Obstacle create(float positionX, boolean overPipe) {
        Vector2 size = new Vector2(1, 1);
        Vector2 position = new Vector2(positionX, getPositionY(overPipe, size));
        Obstacle obstacle = new Obstacle(world, position, size, GlobalProperties.OBSTACLE_TEXTURE);
        obstacle.setDebugName(Integer.toString(obstaclesCounter++));
        return obstacle;
    }

    public float getPositionY(boolean overPipe, Vector2 size) {
        float pipeSemiHeight = GlobalProperties.PIPE_HEIGHT / 2;
        pipeSemiHeight = overPipe ? pipeSemiHeight : -1 * pipeSemiHeight;
        float bodySemiHeight = overPipe ? size.y / 2 : -1*size.y/2;
        return  GlobalProperties.PIPE_Y + pipeSemiHeight + bodySemiHeight;
    }
}
