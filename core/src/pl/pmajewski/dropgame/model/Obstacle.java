package pl.pmajewski.dropgame.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import lombok.Setter;
import pl.pmajewski.dropgame.utils.DebugLogger;
import pl.pmajewski.dropgame.utils.GameManager;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public class Obstacle extends GameActor {

    private TextureRegion textureRegion;
    private Vector2 size;
    @Setter
    private String debugName;

    /*
    * @param position Box2d body position
    * @param size Box2d body size
    * */
    public Obstacle(World world, Vector2 position, Vector2 size, String textureName) {
        super(world);
        this.size = size;
        createBody(world, position);
        setUpTexture(textureName);
    }

    private void createBody(World world, Vector2 position) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(position);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(size.x / 2, size.y / 2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = GlobalProperties.OBSTACLE_DENSITY;
        fixtureDef.restitution = 0f;
        fixtureDef.friction = 0f;

        Body body = world.createBody(bodyDef);
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(this);
        body.setUserData(this);

        shape.dispose();
        this.body = body;
    }

    private void setUpTexture(String textureName) {
        Texture texture = GameManager.getInstance().getAssetManager()
                .get(textureName, Texture.class);
        this.textureRegion = new TextureRegion(texture);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        DebugLogger.debug(500l,"bodyPosition="+body.getPosition()+" angle="+body.getAngle(), "OBSTACLE_"+ debugName);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        drawHelper(batch, textureRegion);
    }

    @Override
    public Float getBodyWidth() {
        return size.x;
    }

    @Override
    public Float getBodyHeight() {
        return size.y;
    }
}
