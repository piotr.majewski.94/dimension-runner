package pl.pmajewski.dropgame.utils;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class GlobalProperties {

    public static final boolean DEBUG_MODE = true;

    public static final float APP_WIDTH = 8;
    public static final float APP_HEIGHT = 16;
    public static final float APP_MIDDLE_Y = APP_HEIGHT / 2;

    public static final float CAM_SHIFT_HORIZONTAL = 2.5f;

    public static final Vector2 WORLD_GRAVITY = new Vector2(0, 0);
    public static final float WORLD_TO_SCREEN = 1;
    public static final float TIME_STEP = 1/60f;

    public static final float PIPE_X = APP_WIDTH / 2;
    public static final float PIPE_Y = APP_MIDDLE_Y;
    public static final float PIPE_WIDTH = APP_WIDTH * 2;
    public static final float PIPE_HEIGHT = 0.15f;

    public static final float PLAYER_WIDTH = 0.75f; // in Box2d world
    public static final float PLAYER_HEIGHT = 0.75f; // in Box2d world
    public static final float PLAYER_X = APP_WIDTH/4;
    public static final float PLAYER_DOWN_Y = APP_MIDDLE_Y - PIPE_HEIGHT / 2 - PLAYER_HEIGHT / 2;
    public static final float PLAYER_TOP_Y = APP_MIDDLE_Y + PIPE_HEIGHT / 2 + PLAYER_HEIGHT / 2;
    public static final float PLAYER_VELOCITY_X = 5f;
    public static final float PLAYER_VELOCITY_Y = 0f;
    public static final float PLAYER_GRAVITY_SCALE = 0f;
    public static final float PLAYER_DENSITY = 1f;
    public static final float PLAYER_MAX_HORIZONTAL_JUMP = 0.5f;
    public static final String PLAYER_TEXTURE = "jump.png";

    public static final float OBSTACLE_DENSITY = 1f;
    public static final String OBSTACLE_TEXTURE = "brick.png";
    public static final float OBSTACLE_LINE_DISTANCE = 0.75f; // box2d metric value
    public static final float OBSTACLE_SHIFT_MIN = PLAYER_WIDTH * 1.5f;
    public static final float OBSTACLE_SHIFT_MAX = 1.5f;

    {
        if(DEBUG_MODE) {
            Gdx.app.setLogLevel(Application.LOG_DEBUG);
        }

    }
}
