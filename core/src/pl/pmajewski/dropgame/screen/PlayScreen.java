package pl.pmajewski.dropgame.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;

import pl.pmajewski.dropgame.stage.GameStage;

public class PlayScreen extends ScreenAdapter {

    private GameStage stage = new GameStage();

    @Override
    public void render(float delta) {
        super.render(delta);

        Gdx.gl.glClearColor(1f/255f*50f, 1f/255f*50f, 1f/255f*50f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glEnable(GL20.GL_SAMPLES);

        stage.draw();
        stage.getBox2DDebug().render(stage.getWorld(), stage.getCamera().combined);
        stage.act(delta);
    }
}
